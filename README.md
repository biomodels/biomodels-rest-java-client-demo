# Demo Java client for BioModels REST API

## Intro

Simple Java client for consuming the [BioModels REST API](https://www.ebi.ac.uk/biomodels/docs). Feel free to use this code as the starting point of your own implementation, or just as inspiration.

BioModels supports both XML and JSON responses, feel free to use whichever you prefer. Here we chose the latter.

## Prerequisites

Java 8 or newer

## Features

* retrieves model information for a given ID
* downloads the model file for a model
* gets all the curated models

## Getting started

Build the self-contained `JAR` file and run it

    # run ./mvnw test if you only want to run the tests
    ./mvnw package
    java -jar target/restClient-0.0.1-SNAPSHOT.jar

We use Maven to build this project. If you don't have it installed, don't worry, it will be downloaded by the wrapper script the first time it runs and then reused subsequently.

### Setting your User Agent

In the spirit of good net citizenship, please set the **[User Agent](https://en.wikipedia.org/wiki/User_agent)** header in your requests. This allows us to understand how our API is used and also to contact you in case of any issues.

Please avoid putting a personally-identifiable email address in your User Agent signature. If you need to do so, our [Privacy Policy](https://www.ebi.ac.uk/data-protection/privacy-notice/embl-ebi-public-website) explains how we safeguard this information.

## Implementation notes

### Dependencies

We use [Apache HttpClient](https://hc.apache.org/) for making REST calls and [Gson](https://github.com/google/gson) for binding JSON responses to domain class instances (`POJO`s).

### High level architecture

#### `demo.App`

This is the main class of the project, showcasing how to retrieve a specific model and all curated models.

#### `demo.BioModelsConnectionService` 

Facade handling communication with BioModels.

It performs requests against the EBI instance of BioModels at `https://www.ebi.ac.uk/biomodels/`. Change the base URI to `http://biomodels.caltech.edu/` if you wish to use the Caltech mirror instead.

Make sure to call its `close()` method in order to avoid resource leaks. One way of doing this is by using the `try-with-resources` idiom:


	try (BioModelsConnectionService bioModelsService = new BioModelsConnectionService()) {
		// get data from BioModels here
	}

Methods implemented here glue two components, both are detailed below:

* an HTTP request that you wish to execute
* a dedicated handler that consumes the response of your request

#### `demo.biomodels.Requests`

This class contains factory methods for creating requests that target different BioModels API endpoints. If you reuse this code, please set the User Agent header in `Requests.constructJsonGetRequest()` to something appropriate for your project.

#### `demo.biomodels.AbstractResponseHandler<T>`

This is the parent superclass for all code to consume responses issued by the BioModels API. The default implementation uses `Gson` to parse a JSON HTTP response and convert it to an instance of domain class `T`.

Concrete subclasses just need to indicate what `T` represents by implementing `Class<T> getObjectMappingClass()`. See `demo.biomodels.GetModelResponseHandler` for an example.

If the API request you are performing does not return a JSON response, you can override the process of turning it into a `POJO` class by implementing `T unmarshallContent(BufferedReader reader, Class<T> type)`. See `demo.biomodels.GetModelFileResponseHandler` for an example.

## Contact

biomodels-developers [at] lists [dot] sourceforge [dot] net

## License

![CC0 Public Domain declaration](https://licensebuttons.net/p/mark/1.0/88x31.png)  [https://creativecommons.org/publicdomain/zero/1.0/](https://creativecommons.org/publicdomain/zero/1.0/)
