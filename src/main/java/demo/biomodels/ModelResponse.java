package demo.biomodels;

/**
 * @author Mihai Glon\u021b mglont@ebi.ac.uk
 */
public class ModelResponse {
    private String name;
    private String submissionId;

    public ModelResponse() {
        // default constructor
    }

    @Override
    public String toString() {
        final StringBuilder response = new StringBuilder();

        response.append("Model id ")
                .append(submissionId)
                .append(" (")
                .append(name)
                .append(")");

        return response.toString();
    }
}
